package com.inmobiliaria.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.inmobiliaria.entity.Inmueble;

@Repository("inmuebleRepository")
public interface InmuebleRepository extends JpaRepository<Inmueble, Serializable>{
	
	public abstract Inmueble findById(int id);
	
}

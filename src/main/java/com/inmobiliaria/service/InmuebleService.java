package com.inmobiliaria.service;

import java.util.List;

import com.inmobiliaria.entity.Inmueble;
import com.inmobiliaria.model.InmuebleModel;

public interface InmuebleService {

	public abstract InmuebleModel addInmueble(InmuebleModel inmuebleModel);
	
	public abstract List<InmuebleModel> listAllInmueble();
	
	public abstract Inmueble findInmuebleById(int id);
	
	public abstract void removeInmueble(InmuebleModel inmuebleModel, int id);
	
	public abstract InmuebleModel findInmuebleByIdModel(int id);
}

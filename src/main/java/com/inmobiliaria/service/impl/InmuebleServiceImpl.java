package com.inmobiliaria.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.inmobiliaria.component.InmuebleConverter;
import com.inmobiliaria.entity.Inmueble;
import com.inmobiliaria.model.InmuebleModel;
import com.inmobiliaria.repository.InmuebleRepository;
import com.inmobiliaria.service.InmuebleService;

@Service("inmuebleServiceImpl")
public class InmuebleServiceImpl implements InmuebleService {

	@Autowired
	@Qualifier("inmuebleRepository")
	private InmuebleRepository inmuebleRepository;

	@Autowired
	@Qualifier("inmuebleConverter")
	private InmuebleConverter inmuebleConverter;

	// método para agregar un inmueble
	@Override
	public InmuebleModel addInmueble(InmuebleModel inmuebleModel) {
		Inmueble inmueble = inmuebleRepository.save(inmuebleConverter.convertInmuebleModel2Contact(inmuebleModel));
		return inmuebleConverter.convertInmuebleToContactModel(inmueble);
	}

	// método para listar los inmuebles
	@Override
	public List<InmuebleModel> listAllInmueble() {
		List<Inmueble> inmuebles = inmuebleRepository.findAll();
		List<InmuebleModel> inmuebleModel = new ArrayList<InmuebleModel>();
		for (Inmueble inmueble : inmuebles) {
			inmuebleModel.add(inmuebleConverter.convertInmuebleToContactModel(inmueble));
		}
		return inmuebleModel;
	}
	
	@Override
	public Inmueble findInmuebleById(int id) {
		return inmuebleRepository.findById(id);
	}

	@Override
	public InmuebleModel findInmuebleByIdModel(int id) {
		return inmuebleConverter.convertInmuebleToContactModel(findInmuebleById(id));

	}

	@Override
	public void removeInmueble(InmuebleModel inmuebleModel, int id) {
		inmuebleModel = inmuebleConverter.convertInmuebleToContactModel(findInmuebleById(id));
		if (null != inmuebleModel) {
			inmuebleModel.setEstado("Inactivo");
			inmuebleRepository.save(inmuebleConverter.convertInmuebleModel2Contact(inmuebleModel));
		}
	}

}

package com.inmobiliaria.component;

import org.springframework.stereotype.Component;

import com.inmobiliaria.entity.Inmueble;
import com.inmobiliaria.model.InmuebleModel;

@Component("inmuebleConverter")
public class InmuebleConverter {

	public Inmueble convertInmuebleModel2Contact(InmuebleModel inmuebleModel) {
		Inmueble inmueble = new Inmueble();
		inmueble.setId(inmuebleModel.getId());
		inmueble.setTitulo(inmuebleModel.getTitulo());
		inmueble.setDescripcion(inmuebleModel.getDescripcion());
		inmueble.setDireccion(inmuebleModel.getDireccion());
		inmueble.setPrecio(inmuebleModel.getPrecio());
		//inmueble.setAsesor(inmuebleModel.getAsesor());
		inmueble.setEstado(inmuebleModel.getEstado());
		inmueble.setFoto1(inmuebleModel.getFoto1());
		inmueble.setFoto2(inmuebleModel.getFoto2());
		inmueble.setFoto3(inmuebleModel.getFoto3());
		return inmueble;
	}

	public InmuebleModel convertInmuebleToContactModel (Inmueble inmueble) {
		InmuebleModel inmuebleModel = new InmuebleModel();
		inmuebleModel.setId(inmueble.getId());
		inmuebleModel.setTitulo(inmueble.getTitulo());
		inmuebleModel.setDescripcion(inmueble.getDescripcion());
		inmuebleModel.setDireccion(inmueble.getDireccion());
		inmuebleModel.setPrecio(inmueble.getPrecio());
		//inmuebleModel.setAsesor(inmueble.getAsesor());
		inmuebleModel.setEstado(inmueble.getEstado());
		inmuebleModel.setFoto1(inmueble.getFoto1());
		inmuebleModel.setFoto2(inmueble.getFoto2());
		inmuebleModel.setFoto3(inmueble.getFoto3());
		return inmuebleModel;
	}
	
}

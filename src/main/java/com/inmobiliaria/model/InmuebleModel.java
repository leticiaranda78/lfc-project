package com.inmobiliaria.model;

public class InmuebleModel {

	private int id;
	private String titulo;
	private String descripcion;
	private int precio;
	private String direccion;
	private String estado;
	private String foto1;
	private String foto2;
	private String foto3;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public int getPrecio() {
		return precio;
	}

	public void setPrecio(int precio) {
		this.precio = precio;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getFoto1() {
		return foto1;
	}

	public void setFoto1(String foto1) {
		this.foto1 = foto1;
	}

	public String getFoto2() {
		return foto2;
	}

	public void setFoto2(String foto2) {
		this.foto2 = foto2;
	}

	public String getFoto3() {
		return foto3;
	}

	public void setFoto3(String foto3) {
		this.foto3 = foto3;
	}

	public InmuebleModel(int id, String titulo, String descripcion, int precio, String direccion, String estado,
			String foto1, String foto2, String foto3) {
		super();
		this.id = id;
		this.titulo = titulo;
		this.descripcion = descripcion;
		this.precio = precio;
		this.direccion = direccion;
		this.estado = estado;
		this.foto1 = foto1;
		this.foto2 = foto2;
		this.foto3 = foto3;
	}

	public InmuebleModel() {

	}

	@Override
	public String toString() {
		return "InmuebleModel [id=" + id + ", titulo=" + titulo + ", descripcion=" + descripcion + ", precio=" + precio
				+ ", direccion=" + direccion + ", estado=" + estado + ", foto1=" + foto1 + ", foto2=" + foto2
				+ ", foto3=" + foto3 + "]";
	}

}

package com.inmobiliaria.constant;

public class ViewConstant {

	//para no cambiar las vistas uno por uno
	public static final String AGREGAR_FORM = "agregar";
	public static final String ADMINISTRAR = "administrar";
	public static final String INDEX = "index";
	public static final String LOGIN = "login";
	public static final String ERROR500 = "error/500";
	
}

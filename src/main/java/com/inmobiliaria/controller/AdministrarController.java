package com.inmobiliaria.controller;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.inmobiliaria.constant.ViewConstant;
import com.inmobiliaria.model.InmuebleModel;
import com.inmobiliaria.service.InmuebleService;

@Controller
@RequestMapping("/administrar")
public class AdministrarController {

	private static final Log LOG = LogFactory.getLog(AdministrarController.class);

	@Autowired
	@Qualifier("inmuebleServiceImpl")
	private InmuebleService inmuebleService;

	// método que cancela la operación
	@GetMapping("/cancel")
	public String cancel() {
		return "redirect:/administrar/showInmuebles";
	}

	// método que lleva a la ventana de agregar
	@GetMapping("/agregar")
	private String redirectAgregarForm(@RequestParam(name = "id", required = false) int id, Model model) {
		InmuebleModel inmuebleModel = new InmuebleModel();
		if (id != 0) {
			inmuebleModel = inmuebleService.findInmuebleByIdModel(id);
		}
		model.addAttribute("inmuebleModel", inmuebleModel);
		return ViewConstant.AGREGAR_FORM;
	}

	// método que agrega el inmueble
	@PostMapping("/addinmueble")
	public String addInmueble(@RequestParam(name = "file") MultipartFile foto1, RedirectAttributes flash,
			@ModelAttribute(name = "inmuebleModel") InmuebleModel inmuebleModel, Model model) {
		LOG.info("METHOD: addInmueble() -- PARAMS:  " + inmuebleModel.toString());

		if (!foto1.isEmpty()) {
			String ruta = "C:\\Users\\usuario\\Documents\\uploads";
			try {

				byte[] bytes1 = foto1.getBytes();

				Path rutaAbsoluta1 = Paths.get(ruta + "/" + foto1.getOriginalFilename());

				Files.write(rutaAbsoluta1, bytes1);

				inmuebleModel.setFoto1(foto1.getOriginalFilename());

			} catch (Exception e) {
				System.out.println(e);
			}
			
			flash.addFlashAttribute("success", "Foto subida!!");
		}
		
		if (null != inmuebleService.addInmueble(inmuebleModel)) {
			model.addAttribute("result", 1);
		} else {
			model.addAttribute("result", 0);
		}

		return "redirect:/administrar/showInmuebles";
	}

	// método para listar los inmuebles
	@GetMapping("/showInmuebles")
	public ModelAndView showInmuebles() {
		ModelAndView mav = new ModelAndView(ViewConstant.ADMINISTRAR);
		mav.addObject("inmuebles", inmuebleService.listAllInmueble());
		return mav;
	}

	// método para eliminar el inmueble
	@GetMapping("removeInmueble")
	public ModelAndView removeInmueble(@RequestParam(name = "id", required = true) int id, InmuebleModel inmuebleModel,
			Model model) {
		
		inmuebleService.removeInmueble(inmuebleModel, id);
		
		if (null != inmuebleService.addInmueble(inmuebleModel)) {
			model.addAttribute("resultado", 1);
		} else {
			model.addAttribute("resultado", 0);
		}
		
		return showInmuebles();
	}

}
